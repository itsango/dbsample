# see http://qiita.com/NAKANO_Akihito/items/9cfc87403e5a4e473eb8

class Hierarchy < ApplicationRecord
  belongs_to :parent, class_name: 'Hierarchy', foreign_key: :parent_id
  has_many :children, class_name: 'Hierarchy', foreign_key: :parent_id
end
