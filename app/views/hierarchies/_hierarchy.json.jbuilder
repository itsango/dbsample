json.extract! hierarchy, :id, :name, :parent_id, :created_at, :updated_at
json.url hierarchy_url(hierarchy, format: :json)