class RenameParenetIdToHierarchies < ActiveRecord::Migration[5.0]
  def change
    rename_column :hierarchies, :parenet_id, :parent_id
  end
end
